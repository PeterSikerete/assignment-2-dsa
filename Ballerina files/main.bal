import ballerina/log;
import ballerinax/kafka;
import ballerinax/mongodb;


kafka:ProducerConfiguration producerConfiguration = {
    clientId: "basic-producer",
    acks: "all",
    retryCount: 3
};
kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL, producerConfiguration);

public function main() {

    mongodb:ClientConfig mongoConfig = {
        host: "localhost",
        port: 9092,
        username: "GrDD",
        password: "GrDD",
        options: {sslEnabled: false, serverSelectionTimeout: 5000}
    };

    mongodb:Client mongoClient = checkpanic new (mongoConfig, "Ballerina");

    map<json> doc1 = { "GroceryCluster": "ballerina", "type": "src" };
    map<json> doc2 = { "GroceryCluster": "connectors", "type": "artifacts" };
    map<json> doc3 = { "GroceryCluster": "docerina", "type": "src" };
    map<json> doc4 = { "GroceryCluster": "test", "type": "artifacts" };

    log:printInfo("------------------ Inserting  Customer Data -------------------");
    checkpanic mongoClient->insert(doc1,"GroceryCluster");
    checkpanic mongoClient->insert(doc2,"GroceryCluster");
    checkpanic mongoClient->insert(doc3,"GroceryCluster");
    checkpanic mongoClient->insert(doc4,"GroceryCluster");
  
    log:printInfo("------------------ Counting Item Quantity  -------------------");
    int count = checkpanic mongoClient->countDocuments("GroceryCluster",());
    log:printInfo("Count of the documents '" + count.toString() + "'.");


    log:printInfo("------------------ Querying Data -------------------");
    map<json>[] jsonRet = checkpanic mongoClient->find("GroceryCluster",(),());
    log:printInfo("Returned documents '" + jsonRet.toString() + "'.");

    map<json> queryString = {"GroceryCluster": "connectors" };
    jsonRet = checkpanic mongoClient->find("GroceryCluster", (), queryString);
    log:printInfo("Returned Filtered documents '" + jsonRet.toString() + "'.");


    log:printInfo("------------------ Updating Item Data -------------------");
    map<json> replaceFilter = { "type": "Item" };
    map<json> replaceDoc = { "name": "main", "type": "Item" };

    int response = checkpanic mongoClient->update(replaceDoc,"GroceryCluster", (), replaceFilter, true);
    if (response > 0 ) {
        log:printInfo("Modified count: '" + response.toString() + "'.") ;
    } else {
        log:printInfo("Error in replacing data");
    }
   log:printInfo("------------------ Order Item  -------------------");
   map<json> orderFilter = { "name": "ballerina" };
   int orderRet = checkpanic mongoClient->delete("GroceryCluster", (), deleteFilter, true);
   if (orderRet > 0 ) {
       log:printInfo("Order count: '" + orderRet.toString() + "'.") ;
   } else {
       log:printInfo("Error in ordering Item");
   }
   log:printInfo("------------------ Deleting Item  -------------------");
   map<json> deleteFilter = { "name": "ballerina" };
   int deleteRet = checkpanic mongoClient->delete("GroceryCluster", (), deleteFilter, true);
   if (deleteRet > 0 ) {
       log:printInfo("Delete count: '" + deleteRet.toString() + "'.") ;
   } else {
       log:printInfo("Error in deleting data");
   }

     mongoClient->close();
}